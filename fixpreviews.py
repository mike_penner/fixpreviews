import sys

if __name__ == "__main__":
    if len(sys.argv) != 2:
        file_path = input("please enter the name of the previews file you'd like to check ")
    else:
        file_path = sys.argv[1]

    try:
        new_file_path = file_path[:file_path.rfind(".")] + ".fixed" + file_path[file_path.rfind("."):]

        file_content = []
        with open(file_path, 'r', encoding="ISO-8859-1") as reader:
            for line in reader.readlines():
                new_line = []
                s = line.split("\t")
                for i in range(5):
                    try:
                        if len(s[i]) < 8 and s[i].find(".") != -1 or s[i] == "0":
                            # is a price, so no quotes
                            new_line.append(s[i])
                        elif s[i].strip('"\n') == "":
                            new_line.append('""')
                        else:
                            # isn't a price
                            new_line.append('"' + s[i].strip('"\n') + '"')
                    except:
                        new_line.append("")

                file_content.append("\t".join(new_line) + "\n")

        with open(new_file_path, 'w') as writer:
            writer.writelines(file_content)
        print(f"wrote your new file to {new_file_path}")

    except:
        print("an error occurred, please run this as:")
        print("python3 fixpreviews.py <optional filename>")
